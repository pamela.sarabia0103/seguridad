<?php
      class Seguridades extends CI_Controller{
        public function __construct(){
            parent::__construct();
              $this->load->model("usuario");




        }
        //funcion que se encargue de renderezar
        //la vista con el formulario de loguin
        public function formularioLogin(){

        $this->load->view("seguridades/formularioLogin");

          $this->load->view("footer");

        }
        //funcion para validar credenciales ingresadas
        public function validarAcceso(){
              $email_usu=$this->input->post("email_usu");
              $password_usu=$this->input->post("password_usu");
              $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
                if ($usuario) {
                  if ($usuario->estado_usu>0) {//existe usuario
                    //validar estado_usu

                    $this->session->set_userdata('c0nectadoUTC',$usuario);
                    $this->session->flashdata('bienvenido',"Ingreso exitoso");
                    redirect("usuarios/index");// la vista que tendra el cliente
                  }
                  else {
                    $this->session->set_flashdata("error","Usuario Inactivo");
                    redirect("Seguridades/formularioLogin");
              }
            }
           else {//datos incorrectos
                $this->session->set_flashdata("error","EMAIL o PASSWORD INCORRECTOS");
                redirect("Seguridades/formularioLogin");
              }

          }
          public function cerrarSesion(){
            $this->session->set_flashdata('salir',"salir");
            $this->session->sess_destroy();//matando sesiones
            redirect("seguridades/formularioLogin");
          }

          public function pruebaEmail(){
            enviarEmail("pamela.sarabia0103@utc.edu.ec","PRUEBA","<H1>HOLA PAMELA SARABIA</H1>");
          }
          public function recuperarPassword(){
            $email=$this->input->post("email");
            $password_aleatorio=rand(111111,999999);
            $asunto="#RECUPERAR PASSWORD";
            $contenido="su contraseña temporal es:<b>$password_aleatorio</b>";
            enviarEmail($email,$asunto,$contenido);
            $this->session->set_flashdata("confirmacion","Hemos enviado una clave temporal a su direccion de email");
            redirect("seguridades/formularioLogin");
          }


}//cierre de clase
