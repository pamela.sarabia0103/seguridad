<?php
      class Clientes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("cliente");
            $this->load->model("pais");
            //validandos si alguien esta conectado
            if($this->session->userdata('c0nectadoUTC')){
              //siesta conectado
            }else{//si no esta conectado ingresa al
              redirect("seguridades/formularioLogin");
            }
        }

        public function index(){
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $this->load->view("header");
          $this->load->view("clientes/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data["listadoPaises"]=$this->pais->consultarTodos();
          $this->load->view("header");
          $this->load->view("clientes/nuevo",$data);
          $this->load->view("footer");
        }
        public function editar($id_cli){
          $data["listadoPaises"]=$this->pais->consultarTodos();
          $data["cliente"]=$this->cliente->consultarPorId($id_cli);
          $this->load->view("header");
          $this->load->view("clientes/editar",$data);
          $this->load->view("footer");
        }
        public function procesarActualizacion(){


          $id_cli=$this->input->post("id_cli");


          $datosClienteEditado=array(
              "identificacion_cli"=>$this->input->post("identificacion_cli"),
              "apellido_cli"=>$this->input->post("apellido_cli"),
              "nombre_cli"=>$this->input->post("nombre_cli"),
              "telefono_cli"=>$this->input->post("telefono_cli"),
              "direccion_cli"=>$this->input->post("direccion_cli"),
              "email_cli"=>$this->input->post("email_cli"),
              "estado_cli"=>$this->input->post("estado_cli"),
              "fk_id_pais"=>$this->input->post("fk_id_pais")

          );
          // logica de negocio necesaria para subir la foto del cliente
          $this->load->library("upload"); //carga e la libreria de subida de archivos
           $nombreTemporal="foto_cliente_".time()."-".rand(1,5000);
           $config["file_name"]="$nombreTemporal";
           $config["upload_path"]=APPPATH.'../uploads/clientes/';
          // validacion solo imagenes
           $config["allowed_types"]="jpeg|jpg|png";
           $config["max_size"]=2*1024;//2MB
           $this->upload->initialize($config);
           //codi  go para subir el archivo y guardar el nombre en la BDD
           if($this->upload->do_upload("foto_cli")){
             $dataSubida=$this->upload->data();
             $datosClienteEditado["foto_cli"]=$dataSubida["file_name"];
             $imagen=$this->cliente->consultarPorId($id_cli);
             $ruta=('./uploads/clientes/'.$imagen->foto_cli);
             unlink($ruta);
           }


          if($this->cliente->actualizar($id_cli,$datosClienteEditado)){
              redirect("clientes/index");
          }else{
              echo "ERROR AL EDITAR EL CLIENTE";
          }

        }

        public function guardarCliente(){
            $datosNuevoCliente=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "direccion_cli"=>$this->input->post("direccion_cli"),
                "email_cli"=>$this->input->post("email_cli"),
                "estado_cli"=>$this->input->post("estado_cli"),
                "fk_id_pais"=>$this->input->post("fk_id_pais")

            );
            // logica de negocio necesaria para subir la foto del cliente
            $this->load->library("upload"); //carga e la libreria de subida de archivos
             $nombreTemporal="foto_cliente_".time()."-".rand(1,5000);
             $config["file_name"]="$nombreTemporal";
             $config["upload_path"]=APPPATH.'../uploads/clientes/';
            // validacion solo imagenes
             $config["allowed_types"]="jpeg|jpg|png";
             $config["max_size"]=2*1024;//2MB
             $this->upload->initialize($config);
             //codi  go para subir el archivo y guardar el nombre en la BDD
             if($this->upload->do_upload("foto_cli")){
               $dataSubida=$this->upload->data();
               $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];
             }

            if($this->cliente->insertar($datosNuevoCliente)){
                $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
            }else{
                $this->session->set_flashdata("confirmacion","Error al procesar, intente nuevamente.");
            }
            redirect("clientes/index");
        }
        function eliminarCliente($id_cli){
      if ($this->session->userdata('c0nectadoUTC')->perfil_usu=="administrador"){
      if($this->cliente->eliminar($id_cli)){

            $this->session->set_flashdata("confirmacion","CLIENTE BORRADO CORRECTAMENTE.");

                  }else{
            $this->session->set_flashdata("error","ERROR AL ELIMINAR, intente nuevamente.");
                  }
      redirect("clientes/index");
    }
  }
  




    }//cierre de la clase
?>
