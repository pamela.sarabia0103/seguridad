<br>
<form action="<?php echo site_url(); ?>/clientes/procesarActualizacion"class="" method="post" enctype="multipart/form-data">
  <h3 align="center">EDITAR CLIENTE</h3>
  <input type="hidden" name="id_cli" id="id_cli" value="<?php echo $cliente->id_cli; ?>">
  <label for="">Pais</label>
  <select class="form-control" name="fk_id_pais" id="fk_id_pais" required style="color:black;">
    <option value="">--Seleccione un pais--</option>
    <?php if ($listadoPaises): ?>
      <?php foreach ($listadoPaises->result() as $paisTemporal): ?>
        <option value="<?php echo $paisTemporal->id_pais; ?>">
          <?php echo $paisTemporal->nombre_pais; ?>
        </option>

      <?php endforeach; ?>

    <?php endif; ?>

  </select>

  <label for="">Identificacion:</label><br>
  <input class="form-control" type="number" name="identificacion_cli" id="identificacion_cli" value="<?php echo $cliente->identificacion_cli; ?>" placeholder="por favor ingresa la identificacion"><br>
  <label for="">Apellido: </label><br>
  <input class="form-control" type="text" name="apellido_cli" id="apellido_cli" value="<?php echo $cliente->apellido_cli; ?>" placeholder="Ingrese el apellido"><br>
  <label for="">Nombre: </label><br>
  <input class="form-control" type="text" name="nombre_cli" id="nombre_cli" value="<?php echo $cliente->nombre_cli; ?>" placeholder="Ingrese el nombre"><br>
  <label for="">Telefono: </label><br>
  <input class="form-control" type="number" name="telefono_cli" id="telefono_cli" value="<?php echo $cliente->telefono_cli; ?>" placeholder="Ingrese su telefono"><br>
  <label for="">Direccion: </label><br>
  <input class="form-control" type="text" name="direccion_cli" id="direccion_cli" value="<?php echo $cliente->direccion_cli; ?>" placeholder="Ingrese su direccion"><br>
  <label for="">Email: </label><br>
  <input class="form-control" type="text" name="email_cli" id="email_cli" value="<?php echo $cliente->email_cli; ?>" placeholder="Ingrese su Email"><br>
  <label for="">Estado: </label><br>

  <select class="form-control" name="estado_cli" id="estado_cli" style="color:black;">
    <option value=""> SELECCIONE UNA OPCION </option>
    <option value="Activo">ACTIVO</option>
    <option value="Inactivo">INACTIVO</option>
  </select>

  <br>
  <br>
  <!-- ingreso de archivos -->

  <label for="">FOTOGRAFIA: </label>
  <br>
<img src="<?php echo  base_url()?>/uploads/clientes/<?php echo $cliente->foto_cli;?>" alt="Sin imagen" height="100px" width="100px" >
  <!-- //validacion solo imagen accept -->
  <input  type="file"   name="foto_cli" accept="image/*" id="foto_cli" value="<?php echo $cliente->foto_cli; ?>">

<!-- fin -->


  <button type="submit" name="button" class="btn btn-warning">GUARDAR</button> &nbsp &nbsp

  <a href="<?php echo site_url() ?>/clientes/index" class="btn btn-danger"> CANCELAR</a>
</form>

<script type="text/javascript">

  //activando el pais seleccionado para el cliente
  $("#fk_id_pais").val("<?php echo $cliente->fk_id_pais; ?>")
  $("#estado_cli").val("<?php echo $cliente->estado_cli; ?>")
  $("#foto_cli").val("<?php echo $cliente->foto_cli; ?>")
</script>
<script type="text/javascript">
   $("#foto_cli").fileinput({
     allowedFileExtensions:["jpeg","jpg","png"],
     dropZoneEnabled:true,
     language:"es"


   });

</script>
