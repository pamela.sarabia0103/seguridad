<br>
<form action="<?php echo site_url(); ?>/clientes/guardarCliente"class="" method="post" id="frm_nuevo_cliente" enctype="multipart/form-data">

  <h3 align="center">AGREGAR NUEVO CLIENTE</h3>
  <label for="">Pais</label>
  <select class="form-control" name="fk_id_pais" id="fk_id_pais" required>
    <option value="">--Seleccione un pais--</option>
    <?php if ($listadoPaises): ?>
      <?php foreach ($listadoPaises->result() as $paisTemporal): ?>
        <option value="<?php echo $paisTemporal->id_pais; ?>">
          <?php echo $paisTemporal->nombre_pais; ?>
        </option>

      <?php endforeach; ?>

    <?php endif; ?>

  </select>
  <br>
  <label for="">Identificacion:</label><br>
  <input class="form-control" type="number" name="identificacion_cli" id="identificacion_cli" value="" placeholder="por favor ingresa la identificacion" ><br>
  <label for="">Apellido: </label><br>
  <input class="form-control" type="text" name="apellido_cli" id="apellido_cli" value="" placeholder="Ingrese el apellido"> <br>
  <label for="">Nombre: </label><br>
  <input class="form-control" type="text" name="nombre_cli" id="nombre_cli" value="" placeholder="Ingrese el nombre"><br>
  <label for="">Telefono: </label><br>
  <input class="form-control" type="number" name="telefono_cli" id="telefono_cli" value="" placeholder="Ingrese su telefono"><br>
  <label for="">Direccion: </label><br>
  <input class="form-control" type="text" name="direccion_cli" id="direccion_cli" value="" placeholder="Ingrese su direccion"><br>
  <label for="">Email: </label><br>
  <input class="form-control" type="text" name="email_cli" id="email_cli" value="" placeholder="Ingrese su Email"><br>

  <label for="">Estado: </label><br>

  <select class="form-control" name="estado_cli" id="estado_cli">
    <option value=""> SELECCIONE UNA OPCION </option>
    <option value="Activo">ACTIVO</option>
    <option value="Inactivo">INACTIVO</option>
  </select>
  <br>
  <br>
  <!-- ingreso de archivos -->
  <label for="">FOTOGRAFIA: </label>
  <!-- //validacion solo imagen accept -->
  <input  type="file" name="foto_cli" accept="image/*" id="foto_cli" value=""><br>
<br>
<br>
<!-- fin -->
  <button type="submit" name="button" class="btn btn-warning">GUARDAR</button> &nbsp &nbsp

  <a href="<?php echo site_url() ?>/clientes/index" class="btn btn-danger"> CANCELAR</a>

</form>
<script type="text/javascript"> $("#frm_nuevo_cliente").validate({
  rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        apellido_cli:{
          letras:true,
          required:true
        },
        nombre_cli:{
          letras:true,
          required:true
        },
        telefono_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        direccion_cli:{
          required:true
        },
        email_cli:{
          required:true,
          email:true
        },
        estado_cli:{
          required:true,

        },
  },
  messages:{
        fk_id_pais:{
          required:"Porfavor seleccione el país"
        },
        identificacion_cli:{
          required:"Porfavor ingrese el numero de cedula",
          minlength:"La cedula debe tener minimo 10 digitos",
          maxlength:"La cedula debe tener maximo 10 digitos",
          digits:"La cedula solo acepta números"
        },
        apellido_cli:{
          letras:"Este campo acepta solo letras",
          required:"Porfavor ingrese su Apellido"
        },
        nombre_cli:{
            letras:"Este campo acepta solo letras",
          required:"Porfavor ingrese su Nombre"
        },
        telefono_cli:{
          required:"Porfavor ingrese su numero de celular",
          minlength:"El celular debe tener minimo 10 digitos",
          maxlength:"El celular debe tener maximo 10 digitos",
          digits:"El celular solo acepta números"
        },
        direccion_cli:{
          required:"Porfavor ingrese su dirección"
        },
        email_cli:{
          required:"Porfavor ingrese su email",
          email:"Ingrese un email válido"
        },
        estado_cli:{
          required:"Porfavor seleccione una opción",

        },
  }
});</script>
<!-- formato JSON -->
<script type="text/javascript">
   $("#foto_cli").fileinput({
     allowedFileExtensions:["jpeg","jpg","png"],
     dropZoneEnabled:true,
     language: "es",
     showCaption: false
   });

</script>
