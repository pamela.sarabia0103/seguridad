<br>
<center>
  <h2>LISTADO DE CLIENTES</h2>
  <hr>
  <br>
  <center>
    <a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-primary"><i class="fa-solid fa-file-pen"></i>Agregar nuevo</a>
    <br><br><br>
    <h3>INFORMACION DE DATOS INGRESADOS</h3>
    <br><br>

<?php if ($listadoClientes): ?>
  <table class="table table-bordered table-striped table-hover display" id="tbl-clientes">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">PAIS</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">INDENTIFICACION</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">OPCIONES</th>

      </tr>


    </thead>
    <tbody>
      <?php foreach ($listadoClientes->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_cli; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->nombre_pais; ?>
          </td>
          <td class="text-center">
            <?php if ($filaTemporal->foto_cli!=""):?>
              <img
              src="<?php echo base_url();?>/uploads/clientes/<?php echo $filaTemporal->foto_cli;?>"
            height="80px"
            width="100px"
            alt="">
          <?php else: ?>
            N/A
          <?php endif; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->apellido_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->telefono_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->direccion_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->email_cli; ?>
          </td>
          <td class="text-center">


            <?php if ($filaTemporal->estado_cli=="Activo"): ?>
              <button type="button" name="button" class="alert alert-success">
                <?php echo $filaTemporal->estado_cli; ?>
              </button>
            <?php else: ?>
              <button type="button" name="button" class="alert alert-danger">
                <?php echo $filaTemporal->estado_cli; ?>
              </button>
            <?php endif; ?>

          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
            <?php if($this->session->userdata("c0nectadoUTC")->perfil_usu=="administrador"):?>

            <a href="javascript:void(0)"onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli; ?>');"
               class="btn btn-danger">
            <i class ="fa fa-trash"></i>
            </a>
          <?php endif; ?>
          </td>
      <?php endforeach; ?>
    </tr>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron clientes registrados</h3>
  </div>
<?php endif; ?>
<script type="text/javascript">
 function confirmarEliminacion(id_cli){
       iziToast.question({
        timeout: 20000,
        close: false,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'CONFIRMACIÓN',
        message: 'Esta seguro de eliminar el cliente de forma permanente?',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href="<?php echo site_url();?>/clientes/eliminarCliente/"+id_cli;
            }, true],
            ['<button>NO</button>', function (instance, toast) {

                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ]
    });

 }

 </script>
<script type="text/javascript">
  $(document).ready(function() {
      listar();

  });

  var listar=function(){
    var tbtabla=$('#tbl-clientes').DataTable({
          'language':es,
          dom: 'Blfrtip',
          buttons: [

              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
    });
  }
  var es={
      "processing": "Procesando...",
      "lengthMenu": "Mostrar _MENU_ registros",
      "zeroRecords": "No se encontraron resultados",
      "emptyTable": "Ningún dato disponible en esta tabla",
      "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "infoFiltered": "(filtrado de un total de _MAX_ registros)",
      "search": "Buscar:",
      "infoThousands": ",",
      "loadingRecords": "Cargando...",
      "paginate": {
          "first": "Primero",
          "last": "Último",
          "next": "Siguiente",
          "previous": "Anterior"
      },
      "aria": {
          "sortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
          "copy": "Copiar",
          "colvis": "Visibilidad",
          "collection": "Colección",
          "colvisRestore": "Restaurar visibilidad",
          "copyKeys": "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
          "copySuccess": {
              "1": "Copiada 1 fila al portapapeles",
              "_": "Copiadas %ds fila al portapapeles"
          },
          "copyTitle": "Copiar al portapapeles",
          "csv": "CSV",
          "excel": "Excel",
          "pageLength": {
              "-1": "Mostrar todas las filas",
              "_": "Mostrar %d filas"
          },
          "pdf": "PDF",
          "print": "Imprimir",
          "renameState": "Cambiar nombre",
          "updateState": "Actualizar",
          "createState": "Crear Estado",
          "removeAllStates": "Remover Estados",
          "removeState": "Remover",
          "savedStates": "Estados Guardados",
          "stateRestore": "Estado %d"
      },
      "autoFill": {
          "cancel": "Cancelar",
          "fill": "Rellene todas las celdas con <i>%d<\/i>",
          "fillHorizontal": "Rellenar celdas horizontalmente",
          "fillVertical": "Rellenar celdas verticalmentemente"
      },
      "decimal": ",",
      "searchBuilder": {
          "add": "Añadir condición",
          "button": {
              "0": "Constructor de búsqueda",
              "_": "Constructor de búsqueda (%d)"
          },
          "clearAll": "Borrar todo",
          "condition": "Condición",
          "conditions": {
              "date": {
                  "after": "Despues",
                  "before": "Antes",
                  "between": "Entre",
                  "empty": "Vacío",
                  "equals": "Igual a",
                  "notBetween": "No entre",
                  "notEmpty": "No Vacio",
                  "not": "Diferente de"
              },
              "number": {
                  "between": "Entre",
                  "empty": "Vacio",
                  "equals": "Igual a",
                  "gt": "Mayor a",
                  "gte": "Mayor o igual a",
                  "lt": "Menor que",
                  "lte": "Menor o igual que",
                  "notBetween": "No entre",
                  "notEmpty": "No vacío",
                  "not": "Diferente de"
              },
              "string": {
                  "contains": "Contiene",
                  "empty": "Vacío",
                  "endsWith": "Termina en",
                  "equals": "Igual a",
                  "notEmpty": "No Vacio",
                  "startsWith": "Empieza con",
                  "not": "Diferente de",
                  "notContains": "No Contiene",
                  "notStarts": "No empieza con",
                  "notEnds": "No termina con"
              },
              "array": {
                  "not": "Diferente de",
                  "equals": "Igual",
                  "empty": "Vacío",
                  "contains": "Contiene",
                  "notEmpty": "No Vacío",
                  "without": "Sin"
              }
          },
          "data": "Data",
          "deleteTitle": "Eliminar regla de filtrado",
          "leftTitle": "Criterios anulados",
          "logicAnd": "Y",
          "logicOr": "O",
          "rightTitle": "Criterios de sangría",
          "title": {
              "0": "Constructor de búsqueda",
              "_": "Constructor de búsqueda (%d)"
          },
          "value": "Valor"
      },
      "searchPanes": {
          "clearMessage": "Borrar todo",
          "collapse": {
              "0": "Paneles de búsqueda",
              "_": "Paneles de búsqueda (%d)"
          },
          "count": "{total}",
          "countFiltered": "{shown} ({total})",
          "emptyPanes": "Sin paneles de búsqueda",
          "loadMessage": "Cargando paneles de búsqueda",
          "title": "Filtros Activos - %d",
          "showMessage": "Mostrar Todo",
          "collapseMessage": "Colapsar Todo"
      },
      "select": {
          "cells": {
              "1": "1 celda seleccionada",
              "_": "%d celdas seleccionadas"
          },
          "columns": {
              "1": "1 columna seleccionada",
              "_": "%d columnas seleccionadas"
          },
          "rows": {
              "1": "1 fila seleccionada",
              "_": "%d filas seleccionadas"
          }
      },
      "thousands": ".",
      "datetime": {
          "previous": "Anterior",
          "next": "Proximo",
          "hours": "Horas",
          "minutes": "Minutos",
          "seconds": "Segundos",
          "unknown": "-",
          "amPm": [
              "AM",
              "PM"
          ],
          "months": {
              "0": "Enero",
              "1": "Febrero",
              "10": "Noviembre",
              "11": "Diciembre",
              "2": "Marzo",
              "3": "Abril",
              "4": "Mayo",
              "5": "Junio",
              "6": "Julio",
              "7": "Agosto",
              "8": "Septiembre",
              "9": "Octubre"
          },
          "weekdays": [
              "Dom",
              "Lun",
              "Mar",
              "Mie",
              "Jue",
              "Vie",
              "Sab"
          ]
      },
      "editor": {
          "close": "Cerrar",
          "create": {
              "button": "Nuevo",
              "title": "Crear Nuevo Registro",
              "submit": "Crear"
          },
          "edit": {
              "button": "Editar",
              "title": "Editar Registro",
              "submit": "Actualizar"
          },
          "remove": {
              "button": "Eliminar",
              "title": "Eliminar Registro",
              "submit": "Eliminar",
              "confirm": {
                  "_": "¿Está seguro que desea eliminar %d filas?",
                  "1": "¿Está seguro que desea eliminar 1 fila?"
              }
          },
          "error": {
              "system": "Ha ocurrido un error en el sistema (<a target=\"\\\" rel=\"\\ nofollow\" href=\"\\\">Más información&lt;\\\/a&gt;).<\/a>"
          },
          "multi": {
              "title": "Múltiples Valores",
              "info": "Los elementos seleccionados contienen diferentes valores para este registro. Para editar y establecer todos los elementos de este registro con el mismo valor, hacer click o tap aquí, de lo contrario conservarán sus valores individuales.",
              "restore": "Deshacer Cambios",
              "noMulti": "Este registro puede ser editado individualmente, pero no como parte de un grupo."
          }
      },
      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
      "stateRestore": {
          "creationModal": {
              "button": "Crear",
              "name": "Nombre:",
              "order": "Clasificación",
              "paging": "Paginación",
              "search": "Busqueda",
              "select": "Seleccionar",
              "columns": {
                  "search": "Búsqueda de Columna",
                  "visible": "Visibilidad de Columna"
              },
              "title": "Crear Nuevo Estado",
              "toggleLabel": "Incluir:"
          },
          "emptyError": "El nombre no puede estar vacio",
          "removeConfirm": "¿Seguro que quiere eliminar este %s?",
          "removeError": "Error al eliminar el registro",
          "removeJoiner": "y",
          "removeSubmit": "Eliminar",
          "renameButton": "Cambiar Nombre",
          "renameLabel": "Nuevo nombre para %s",
          "duplicateError": "Ya existe un Estado con este nombre.",
          "emptyStates": "No hay Estados guardados",
          "removeTitle": "Remover Estado",
          "renameTitle": "Cambiar Nombre Estado"
      }
  }
</script>
