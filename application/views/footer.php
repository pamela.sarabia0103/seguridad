</div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
      <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin templates</a> from Bootstrapdash.com</span>
    </div>
  </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- base:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="vendors/chart.js/Chart.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <script type="text/javascript">
   function confirmarSalir(Salir){
         iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'CONFIRMACIÓN',
          message: 'Esta seguro de cerrar Sesión?',
          position: 'center',
          buttons: [
              ['<button><b>SI</b></button>', function (instance, toast) {
                  instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                  window.location.href="<?php echo site_url('seguridades/cerrarSesion');?>";
              }, true],
              ['<button>NO</button>', function (instance, toast) {

                  instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

              }],
          ]
      });

   }

   </script>
  <!-- End custom js for this page-->
  <?php if ($this->session->flashdata("confirmacion")): ?>
    <script type="text/javascript">
      iziToast.success({
        title:'CONFIRMACION',
        message: '<?php echo $this->session->flashdata("confirmacion") ?>',
        position: 'topRight',

      });
    </script>
  <?php endif; ?>
  <?php if ($this->session->flashdata("salir")): ?>
    <script type="text/javascript">
      iziToast.success({
        title:'Salir',
        message: '<?php echo $this->session->flashdata("salir") ?>',
        position: 'topRight',

      });
    </script>
  <?php endif; ?>

  <?php if ($this->session->flashdata("error")): ?>
    <script type="text/javascript">
      iziToast.danger({
        title:'ADVERTENCIA',
        message: '<?php echo $this->session->flashdata("error") ?>',
        position: 'topRight',

      });
    </script>
  <?php endif; ?>

  <?php if ($this->session->flashdata("bienvenido")): ?>
    <script type="text/javascript">
      iziToast.info({
           title: 'CONFIRMACIÓN',
           message: '<?php echo $this->session->flashdata("bienvenido"); ?>',
           position: 'center',
         });
    </script>
  <?php endif; ?>

<style media="screen">
    .error{
      color:red;
        font-size: 16px;

      }

    input.error, select.error{
      border:2px solid red !important;
      }

</style>
      <script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>

</body>

</html>
