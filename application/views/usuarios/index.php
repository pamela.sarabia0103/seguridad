<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <br>
  <h1 class="text-center">GESTIÓN DE USUARIOS</h1>
  <div class="row">
    <div class="col-md-12 " id="contenedor-listado-usuarios">
      <i class="fa fa-spin fa-lg fa-spinner"></i>
        Consultando Datos
      </div>
  </div>
      <br>


<!-- inicio editar modal -->
      <div id="ModalEditarUsuario" style="z-index:9999 !important;" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-users"></i>EDITAR USUARIO</h4>
              <button type="button" class="close"  data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="contenedor-formulario-editar">

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>

        </div>
      </div>
      <!-- fin eidtar modal -->



<!-- valida json -->
<script type="text/javascript">
 function  cargarListadoUsuarios() {
   $("#contenedor-listado-usuarios").html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
  $("#contenedor-listado-usuarios").load("<?php echo site_url();?>/usuarios/listado");
 }
 cargarListadoUsuarios();

 $("#frm_nuevo_usuario").validate({
     rules:{
       apellido_usu:{
         required:true
       },
       nombre_usu:{
         letras:true,
         required:true
       },
        email_usu:{
         required:true,
         email:true
       },
       password_usu:{
         required:true
       },
       password_confirmada:{
         required:true,
         equalTo:"#password_usu"

       },
       perfil_usu:{
         required:true
       }
     },
     messages:{
       apellido_usu:{
         required:"Porfavor ingrese su apellido"
       },
       nombre_usu:{
           letras:"Este campo acepta solo letras",
         required:"Porfavor ingrese su Nombre"
       },
         email_usu:{
         required:"Porfavor ingrese su email",
         email:"Ingrese un email válido"
       },
       password_usu:{
         required:"porfavor ingrese la contraseña"
       },
       password_confirmada:{
         required:"porfavor ingrese la contraseña , No coinciden"
       },
       perfil_usu:{
         required:"Seleccione una opción"
       }

     },
     // funcin para peticiones ajax
     submitHandler:function(form){
       $.ajax({
         url:$(form).prop("action"),
         type:"post",
         data:$(form).serialize(),
         success:function(data){
           cargarListadoUsuarios();
           $("#ModalNuevoUsuario").modal("hide");
           //backdrop
           alert(data);
         }
       });
     }
   });

</script>
<script type="text/javascript">
 function abrirFormularioEditar(id_usu){

    $("#contenedor-formulario-editar").html('i class="fa fa-spin fa-lg fa-spinner"></i>');
    $("#contenedor-formulario-editar").load("<?php echo site_url('usuarios/editar'); ?>/"+id_usu);
    $("#ModalEditarUsuario").modal("show");


    alert("ok...");
    }

</script>
