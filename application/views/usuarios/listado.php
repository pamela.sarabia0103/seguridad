
<!-- Trigger the modal with a button -->

<?php if ($listadoUsuarios): ?>
  <table class="table table-bordered table-striped table-hover display" id="tbl-usuarios">
      <thead>
         <tr>
           <th class="text-center">ID</th>
           <th class="text-center">APELLIDO</th>
           <th class="text-center">NOMBRE</th>
             <th class="text-center">CATEGORIA</th>
           <th class="text-center">EMAIL</th>
           <th class="text-center">PERFIL</th>
            <th class="text-center">ACCIONES</th>
         </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoUsuarios->result() as $usuario): ?>
              <tr>
                  <td class="text-center">
                    <?php echo $usuario->id_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->apellido_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->nombre_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->email_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->perfil_usu; ?>
                  </td>
                  <td class="text-center">
                    <a href="#"
                    onclick="abrirFormularioEditar(<?php echo $usuario->id_usu; ?>);"
                    class="btn btn-warning">
                      <i class="fa fa-edit"></i>
                    </a>
                    &nbsp;
                    <a href="#" class="btn btn-danger">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
<?php else: ?>
    <br>
    <div class="alert alert-danger">
        No se encontraron Usuarios Registrados
    </div>
<?php endif; ?>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal"  data-target="#ModalNuevoUsuario" >Agregar Usuario</button>
<button class="btn btn-success" type="button" name="button" onclick="cargarListadoUsuarios();">Actualizar</button>
<div id="ModalNuevoUsuario" style="z-index:9999 !important;" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-users"></i>NUEVO USUARIO</h4>
        <button type="button" class="close"  data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <!-- guarda los datos -->
      <form class="" action="<?php echo site_url('usuarios/insertarUsuario'); ?>" method="post" id="frm_nuevo_usuario">
          <br>
          <label for="">Apellido: </label><br>
           <br>
          <input class="form-control" type="text" required name="apellido_usu" id="apellido_usu" value="" placeholder="Ingrese el apellido"> <br>
          <label for="">Nombre: </label><br>
          <input class="form-control" type="text" name="nombre_usu" id="nombre_usu" value="" placeholder="Ingrese el nombre"><br>
          <label for="">Email: </label><br>
          <input class="form-control" type="text" name="email_usu" id="email_usu" value="" placeholder="Ingrese su Email"><br>
          <label for="">Contraseña: </label><br>
          <input class="form-control" type="password" name="password_usu" id="password_usu" value="" placeholder="Ingrese su Contraseña"><br>
          <label for="">Confirme la contraseña: </label><br>
          <input class="form-control" type="password" name="password_confirmada" id="password_confirmada" value="" placeholder="Ingrese su Contraseña"><br>
          <label for="">Perfil: </label><br>
          <select class="form-control" name="perfil_usu"  id="perfil_usu" placeholder="Ingrese su Perfil" ><br>
            <option value="ADMINISTRADOR">ADMINISTRADOR</option>
            <option value="VENDEDOR">VENDEDOR</option>
          </select>
          <script type="text/javascript">
          $("#perfil_usu_editar").val("<?php echo $usuario->perfil_usu; ?>")

          </script>
        <br>
        <!-- fin -->
          <button type="submit" name="button" class="btn btn-success">GUARDAR</button> &nbsp &nbsp

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      listar();
  });

  var listar=function(){
    var tbtabla=$('#tbl-usuarios').DataTable({
          'language':es,
          dom: 'Blfrtip',

          buttons: [

              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
    });
  }
  var es={
      "processing": "Procesando...",
      "lengthMenu": "Mostrar _MENU_ registros",
      "zeroRecords": "No se encontraron resultados",
      "emptyTable": "Ningún dato disponible en esta tabla",
      "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "infoFiltered": "(filtrado de un total de _MAX_ registros)",
      "search": "Buscar:",
      "infoThousands": ",",
      "loadingRecords": "Cargando...",
      "paginate": {
          "first": "Primero",
          "last": "Último",
          "next": "Siguiente",
          "previous": "Anterior"
      },
      "aria": {
          "sortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      "buttons": {
          "copy": "Copiar",
          "colvis": "Visibilidad",
          "collection": "Colección",
          "colvisRestore": "Restaurar visibilidad",
          "copyKeys": "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
          "copySuccess": {
              "1": "Copiada 1 fila al portapapeles",
              "_": "Copiadas %ds fila al portapapeles"
          },
          "copyTitle": "Copiar al portapapeles",
          "csv": "CSV",
          "excel": "Excel",
          "pageLength": {
              "-1": "Mostrar todas las filas",
              "_": "Mostrar %d filas"
          },
          "pdf": "PDF",
          "print": "Imprimir",
          "renameState": "Cambiar nombre",
          "updateState": "Actualizar",
          "createState": "Crear Estado",
          "removeAllStates": "Remover Estados",
          "removeState": "Remover",
          "savedStates": "Estados Guardados",
          "stateRestore": "Estado %d"
      },
      "autoFill": {
          "cancel": "Cancelar",
          "fill": "Rellene todas las celdas con <i>%d<\/i>",
          "fillHorizontal": "Rellenar celdas horizontalmente",
          "fillVertical": "Rellenar celdas verticalmentemente"
      },
      "decimal": ",",
      "searchBuilder": {
          "add": "Añadir condición",
          "button": {
              "0": "Constructor de búsqueda",
              "_": "Constructor de búsqueda (%d)"
          },
          "clearAll": "Borrar todo",
          "condition": "Condición",
          "conditions": {
              "date": {
                  "after": "Despues",
                  "before": "Antes",
                  "between": "Entre",
                  "empty": "Vacío",
                  "equals": "Igual a",
                  "notBetween": "No entre",
                  "notEmpty": "No Vacio",
                  "not": "Diferente de"
              },
              "number": {
                  "between": "Entre",
                  "empty": "Vacio",
                  "equals": "Igual a",
                  "gt": "Mayor a",
                  "gte": "Mayor o igual a",
                  "lt": "Menor que",
                  "lte": "Menor o igual que",
                  "notBetween": "No entre",
                  "notEmpty": "No vacío",
                  "not": "Diferente de"
              },
              "string": {
                  "contains": "Contiene",
                  "empty": "Vacío",
                  "endsWith": "Termina en",
                  "equals": "Igual a",
                  "notEmpty": "No Vacio",
                  "startsWith": "Empieza con",
                  "not": "Diferente de",
                  "notContains": "No Contiene",
                  "notStarts": "No empieza con",
                  "notEnds": "No termina con"
              },
              "array": {
                  "not": "Diferente de",
                  "equals": "Igual",
                  "empty": "Vacío",
                  "contains": "Contiene",
                  "notEmpty": "No Vacío",
                  "without": "Sin"
              }
          },
          "data": "Data",
          "deleteTitle": "Eliminar regla de filtrado",
          "leftTitle": "Criterios anulados",
          "logicAnd": "Y",
          "logicOr": "O",
          "rightTitle": "Criterios de sangría",
          "title": {
              "0": "Constructor de búsqueda",
              "_": "Constructor de búsqueda (%d)"
          },
          "value": "Valor"
      },
      "searchPanes": {
          "clearMessage": "Borrar todo",
          "collapse": {
              "0": "Paneles de búsqueda",
              "_": "Paneles de búsqueda (%d)"
          },
          "count": "{total}",
          "countFiltered": "{shown} ({total})",
          "emptyPanes": "Sin paneles de búsqueda",
          "loadMessage": "Cargando paneles de búsqueda",
          "title": "Filtros Activos - %d",
          "showMessage": "Mostrar Todo",
          "collapseMessage": "Colapsar Todo"
      },
      "select": {
          "cells": {
              "1": "1 celda seleccionada",
              "_": "%d celdas seleccionadas"
          },
          "columns": {
              "1": "1 columna seleccionada",
              "_": "%d columnas seleccionadas"
          },
          "rows": {
              "1": "1 fila seleccionada",
              "_": "%d filas seleccionadas"
          }
      },
      "thousands": ".",
      "datetime": {
          "previous": "Anterior",
          "next": "Proximo",
          "hours": "Horas",
          "minutes": "Minutos",
          "seconds": "Segundos",
          "unknown": "-",
          "amPm": [
              "AM",
              "PM"
          ],
          "months": {
              "0": "Enero",
              "1": "Febrero",
              "10": "Noviembre",
              "11": "Diciembre",
              "2": "Marzo",
              "3": "Abril",
              "4": "Mayo",
              "5": "Junio",
              "6": "Julio",
              "7": "Agosto",
              "8": "Septiembre",
              "9": "Octubre"
          },
          "weekdays": [
              "Dom",
              "Lun",
              "Mar",
              "Mie",
              "Jue",
              "Vie",
              "Sab"
          ]
      },
      "editor": {
          "close": "Cerrar",
          "create": {
              "button": "Nuevo",
              "title": "Crear Nuevo Registro",
              "submit": "Crear"
          },
          "edit": {
              "button": "Editar",
              "title": "Editar Registro",
              "submit": "Actualizar"
          },
          "remove": {
              "button": "Eliminar",
              "title": "Eliminar Registro",
              "submit": "Eliminar",
              "confirm": {
                  "_": "¿Está seguro que desea eliminar %d filas?",
                  "1": "¿Está seguro que desea eliminar 1 fila?"
              }
          },
          "error": {
              "system": "Ha ocurrido un error en el sistema (<a target=\"\\\" rel=\"\\ nofollow\" href=\"\\\">Más información&lt;\\\/a&gt;).<\/a>"
          },
          "multi": {
              "title": "Múltiples Valores",
              "info": "Los elementos seleccionados contienen diferentes valores para este registro. Para editar y establecer todos los elementos de este registro con el mismo valor, hacer click o tap aquí, de lo contrario conservarán sus valores individuales.",
              "restore": "Deshacer Cambios",
              "noMulti": "Este registro puede ser editado individualmente, pero no como parte de un grupo."
          }
      },
      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
      "stateRestore": {
          "creationModal": {
              "button": "Crear",
              "name": "Nombre:",
              "order": "Clasificación",
              "paging": "Paginación",
              "search": "Busqueda",
              "select": "Seleccionar",
              "columns": {
                  "search": "Búsqueda de Columna",
                  "visible": "Visibilidad de Columna"
              },
              "title": "Crear Nuevo Estado",
              "toggleLabel": "Incluir:"
          },
          "emptyError": "El nombre no puede estar vacio",
          "removeConfirm": "¿Seguro que quiere eliminar este %s?",
          "removeError": "Error al eliminar el registro",
          "removeJoiner": "y",
          "removeSubmit": "Eliminar",
          "renameButton": "Cambiar Nombre",
          "renameLabel": "Nuevo nombre para %s",
          "duplicateError": "Ya existe un Estado con este nombre.",
          "emptyStates": "No hay Estados guardados",
          "removeTitle": "Remover Estado",
          "renameTitle": "Cambiar Nombre Estado"
      }
  }
</script>
